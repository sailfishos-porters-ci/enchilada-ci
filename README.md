# Sailfish OS on OnePlus 6

* 3.3.0.16 - [![pipeline status](https://gitlab.com/sailfishos-porters-ci/enchilada-ci/badges/3.3.0.16/pipeline.svg)](https://gitlab.com/sailfishos-porters-ci/cheeseburger-ci/commits/3.3.0.16)

This repository uses [GitLab's Continuous Integration](https://docs.gitlab.com/ee/ci) to build Sailfish OS images for the OnePlus 6.

![OnePlus 6](https://wiki.merproject.org/w/images/d/d4/Sailfish-oneplus6.png "OnePlus 6")

## Device specifications

Part         | Specification
------------:|:-------------
Chipset      | Qualcomm Snapdragon 845 (SDM845)
CPU          | Octa-Core Kryo 280 (4x 2.8 GHz Cortex A73 & 4x 1.80 GHz Cortex A53)
Arch         | ARMv8-A 64-bit / aarch64 / arm64
GPU          | Qualcomm Adreno 630
RAM          | 6 or 8 GB LPDDR4X
Storage      | 64 or 128 GB UFS 2.1
Android      | 8.1.0, up to 10.1 (OxygenOS)
Kernel       | Linux 4.9.x series
Battery      | 3 100 mAh Li-Po
Display      | 1080 x 2280 pixels, 5.5" Optic AMOLED
Rear Cameras | 16 MP, f/1.7, 25mm (wide), 1/2.6", 1.22µm, PDAF, OIS + 20 MP (16 MP effective), f/1.7, 25mm (wide), 1/2.8", 1.0µm, PDAF
Front Camera | 16 MP, f/2.0, 25mm (wide), 1/3.06", 1.0µm

A bunch of the other specifics can be found [here](https://www.gsmarena.com/oneplus_6-9109.php).

# Files

[Jolla-@RELEASE@-enchilada-@ARCH@.ks](Jolla-@RELEASE@-cheeseburger-@ARCH@.ks) is the kickstart file used by the PlatformSDK image. The file contains device specific repositories, which can be changed by from `devel` to `testing` or vice versa.

[run-mic.sh](run-mic.sh) is a simple bash script which executes the `mic` build to build the rootfs and installer zip.

# Download

[Download the latest build](https://gitlab.com/sailfishos-porters-ci/enchilada-ci/-/jobs?scope=finished)

# Source code

[https://github.com/sailfish-oneplus6](https://github.com/sailfish-oneplus6)


Shamelessly stolen from @deathmist and friends. [cheeseburger-ci](https://gitlab.com/sailfishos-porters-ci/cheeseburger-ci).
