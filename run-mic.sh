#!/bin/bash
export PLATFORM_SDK_ROOT="/srv/mer"
export ANDROID_ROOT="/parentroot/srv/hadk"
export VENDOR="oneplus"
export DEVICE="$1"
export PORT_ARCH="armv7hl"
export RELEASE="3.3.0.16"

echo "================================"
echo "Building installer for: $DEVICE"
echo "================================"
echo

sudo mic create fs --arch=$PORT_ARCH \
                   --tokenmap=ARCH:$PORT_ARCH,DEVICE:$DEVICE,RELEASE:$RELEASE,EXTRA_NAME:"$EXTRA_NAME" \
                   --record-pkgs=name,url \
                   --outdir=sfe-$DEVICE-$RELEASE"$EXTRA_NAME" \
                   --pack-to=sfe-$DEVICE-$RELEASE"$EXTRA_NAME".tar.bz2 \
                   Jolla-@RELEASE@-oneplus-sdm845-@ARCH@.ks
